package Server;

import java.util.Date;

public class Ticket {
	/*
	 *  A ticket contains information such as 
		a.  Passenger first and last name 
		b.  Flight number 
		c.  Flight source and destination 
		d.  Flight date, time and duration 
		e.  Price of the ticket 
		f.	Unique ID of ticket
	 */
	final static double GST=1.07;
	private int id;
	private PassengerInfo passenger;
	private int flightNum;
	private String flightSrc;
	private String flightDest;
	private Date flightDate;
	private int flightDuration;
	private double ticketPrice;
	
	Ticket() {
		PassengerInfo passenger = new PassengerInfo();
		
	}
	Ticket(String firstname, String lastname, String birthday, 
			int flightNum, String source, String destination,
			Date departureTime, int durationMinutes, double price) {
		PassengerInfo passenger = new PassengerInfo();
		passenger.setFirstname(firstname);
		passenger.setLastname(lastname);
		passenger.setBirthday(birthday);
		
		this.flightNum = flightNum;
		this.flightSrc = source;
		this.flightDest = destination;
		this.flightDate = departureTime;
		this.flightDuration = durationMinutes;
		this.ticketPrice = price * GST;
	}
	
	public int getId() {
		return id;
	}

	public PassengerInfo getPassenger() {
		return passenger;
	}

	public int getFlightNum() {
		return flightNum;
	}

	public String getFlightSrc() {
		return flightSrc;
	}

	public String getFlightDest() {
		return flightDest;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public int getFlightDuration() {
		return flightDuration;
	}

	public double getTicketPrice() {
		return ticketPrice;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPassenger(PassengerInfo passenger) {
		this.passenger = passenger;
	}

	public void setFlightNum(int flightNum) {
		this.flightNum = flightNum;
	}

	public void setFlightSrc(String flightSrc) {
		this.flightSrc = flightSrc;
	}

	public void setFlightDest(String flightDest) {
		this.flightDest = flightDest;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public void setFlightDuration(int flightDuration) {
		this.flightDuration = flightDuration;
	}

	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public void printTicket() {
		System.out.println("Firstname: " + passenger.getFirstname() +
							"Lastname: " + passenger.getLastname() +
							"Date of birth:" + passenger.getBirthday() +
							"Flight Source: " + flightSrc +
							"Flight Destination: " + flightDest +
							"Flight date: " + flightDate +
							"Flight duration: " + flightDuration +
							"Ticket price: " + ticketPrice);
	}
	
	public String toString() {
		return ("Firstname: " + passenger.getFirstname() +
				"Lastname: " + passenger.getLastname() +
				"Date of birth:" + passenger.getBirthday() +
				"Flight Source: " + flightSrc +
				"Flight Destination: " + flightDest +
				"Flight date: " + flightDate +
				"Flight duration: " + flightDuration +
				"Ticket price: " + ticketPrice);
	}

}
