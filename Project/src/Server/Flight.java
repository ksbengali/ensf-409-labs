package Server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Flight {
	/*
	 * Each Flight contains the following information  
		-  Flight number (which is unique) 
		-  Source and destination locations  
		-  Date, time and duration of the flight 
		-  Total number of seats available 
		-  Remaining number of seats 
		-  Price of the flight
	 */
	private ArrayList<Ticket> tickets;
	private int flightNum;
	private String source;
	private String destination;
	private Date departureTime;
	private int durationMinutes;
	private int totalSeats;
	private int remainingSeats;
	private double price;
	private SimpleDateFormat dt = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
	
	Flight() throws ParseException {
		tickets = new ArrayList<Ticket>();
		flightNum = 0;
		//source = destination = null;
		source = "Calgary";
		destination = "Toronto";
		//departureTime = null;
		String departure_time = "1/1/2017 12:45:00";
		departureTime = dt.parse(departure_time);
		totalSeats  = remainingSeats = 0;
		durationMinutes = 0;
		price = 0;
	}
	
	Flight(int flightNum, String source, String destination,
			Date departureTime, int durationMinutes, int totalSeats, double price) {
		this.flightNum = flightNum;
		this.source = source;
		this.destination = destination;
		this.departureTime = departureTime;
		this.durationMinutes = durationMinutes;
		this.totalSeats = totalSeats;
		this.remainingSeats = this.totalSeats;
		this.price = price;
		tickets = new ArrayList<Ticket>(totalSeats);
		
	}
	
	public int getFlightNum() {
		return flightNum;
	}

	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public int getDurationMinutes() {
		return durationMinutes;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public int getRemainingSeats() {
		return remainingSeats;
	}

	public double getPrice() {
		return price;
	}

	public ArrayList<Ticket> getTickets() {
		return tickets;
	}

	public void setFlightNum(int flightNum) {
		this.flightNum = flightNum;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public void setDurationMinutes(int durationMinutes) {
		this.durationMinutes = durationMinutes;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public void setRemainingSeats(int remainingSeats) {
		this.remainingSeats = remainingSeats;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setTickets(ArrayList<Ticket> tickets) {
		this.tickets = tickets;
	}

	public void addTicket(String firstname, String lastname, String birthday) {
		if (remainingSeats > 0) {
			// Read in firstname, lastname, birthday of passenger
			// TODO: Initialize from GUI
			//String firstname = null;
			//String lastname = null;
			//Date birthday = null;
			Ticket tempTicket = new Ticket(firstname, lastname, birthday, flightNum,
					source, destination, departureTime, durationMinutes, price);
			tickets.add(tempTicket);
			System.out.println(tempTicket.toString());
			remainingSeats--;
		}
		return; // no seats left
	}

	public void deleteTicket(int ticketID) {
		Iterator<Ticket> iterator = tickets.iterator();
		while(iterator.hasNext())
		{
		    int value = iterator.next().getId();
		    if (ticketID == value)
		    {
		        iterator.remove();
		        break;
		    }
		}
	}

}
