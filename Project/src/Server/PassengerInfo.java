package Server;

import java.util.Date;

class PassengerInfo {
	private String firstname;
	private String lastname;
	private String birthday;
	
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
}
