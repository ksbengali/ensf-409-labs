package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.DefaultListModel;

public class FlightCatalogue {

	PrintWriter out;
	Socket aSocket;
	BufferedReader in;
	//private DBConnector database;
	private ArrayList<Flight> flights;
	
	private ServerSocket serverSocket;
	private ExecutorService pool;
	
	FlightCatalogue(int port) {
		try {
			serverSocket = new ServerSocket(port);
			pool = Executors.newCachedThreadPool();
			System.out.println("Server is running");
			flights = new ArrayList<Flight>();
		} catch (IOException e) {
			System.out.println("Could not start server");
			e.printStackTrace();
		}
		
	}
	/*public FlightCatalogue(ArrayList<Flight> _flights) {
		try {
			serverSocket = new ServerSocket(9898);
		} catch (IOException e) {
			System.out.println("Create the new socket error");
		}
		System.out.println("Server is running");
		this.flights = _flights;
	}*/
	
	public static void main(String[] args) throws IOException {
		FlightCatalogue server = new FlightCatalogue(9898);
		try {
			for(;;){
				server.pool.execute(server.new WorkerThread(server.serverSocket.accept()));
			}
		} catch (IOException e) {
			server.pool.shutdown();
		}
	}
	
	// Assume when adding flights, all seats are available
	public void addFlight(int flightNum, String source, String destination,
			Date departureTime, int durationMinutes, int totalSeats, double price) {
		Flight tempFlight = new Flight(flightNum, source, destination, 
				departureTime, durationMinutes, totalSeats, price);
		flights.add(tempFlight);
		// TODO: Update database with new entry
	}
	
	public void deleteFlight(int flightNum) {
		Iterator<Flight> iterator = flights.iterator();
		while(iterator.hasNext())
		{
		    int value = iterator.next().getFlightNum();
		    if (flightNum == value)
		    {
		        iterator.remove();
		        break;
		    }
		}
		//TODO: Update database
		
	}
	
	public void searchFlights(String date, String origin, String destination){
		// TODO: Search flights from database
	}
	
	public boolean bookTicket(int id, String firstName, String lastName, String birthday){
		for(Flight f:flights){
			if(f.getFlightNum() == id){
				f.addTicket(firstName, lastName, birthday);
				return true;
			}
		}
		return false;
	}
	
	public class WorkerThread implements Runnable{
		private Socket 				aSocket = null;
		private BufferedReader 		socketIn;
		private PrintWriter 		socketOut;
		private ObjectInputStream 	objectIn;
		private ObjectOutputStream 	objectOut;
		
		public WorkerThread(Socket s){
			aSocket = s;
			try{
				socketIn	= 	new	BufferedReader(new InputStreamReader(aSocket.getInputStream()));
				socketOut	=	new PrintWriter((aSocket.getOutputStream()),true);
				objectIn	=	new	ObjectInputStream(aSocket.getInputStream());
				objectOut	=	new ObjectOutputStream(aSocket.getOutputStream());
			}catch(IOException e){
				System.out.println("Error, could not establish server connection");
				e.printStackTrace();
			}
		}
		
		public ArrayList<Flight> searchFlight(String date, String origin, String destination) throws ParseException{
			flights.clear();
			flights = new ArrayList<Flight>();
			Flight newFlight = new Flight();
			flights.add(newFlight);
			// TODO: Search database
			return flights;
		}
		
		@Override
		public void run() {
			String line = null;
			try{
				while(true){
					line = socketIn.readLine();
					if(line.equalsIgnoreCase("QUIT")){
						System.out.println("Server is quitting ...");
						break;
					}else if(line.equalsIgnoreCase("SEARCH_FLIGHTS")){
						String date		=socketIn.readLine();
						String origin	=socketIn.readLine();
						String dest		=socketIn.readLine();
						
						DefaultListModel<String> returnSearch = new DefaultListModel<String>();
						String message = null;
						for(Flight f:flights){
							message =  f.getFlightNum() + " ; " + f.getDestination() + " ; " + f.getSource() + " ; " + f.getDepartureTime() + " ; " + f.getDurationMinutes() + " ; " + f.getTotalSeats() + " ; " + f.getRemainingSeats() + " ; " + f.getPrice();
							returnSearch.addElement(message);
						}
						
						objectOut.flush();
						objectOut.writeObject(returnSearch);
					}else if(line.equalsIgnoreCase("BOOK_TICKET")){
						int 	flightId 		= Integer.parseInt(socketIn.readLine());
						String 	firstName 		= socketIn.readLine();
						String 	lastName 		= socketIn.readLine();
						String 	birthday		= socketIn.readLine();
						
						if(bookTicket(flightId, firstName, lastName, birthday)){
							socketOut.flush();
							socketOut.println("PASS");
						}else{
							socketOut.flush();
							socketOut.println("FAIL");
						}
					}
					// TODO: Add more cases
				}
				socketIn.close();
				socketOut.close();
				objectIn.close();
				objectOut.close();
				aSocket.close();
			}catch(SocketException socketError){
				socketError.printStackTrace();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}
