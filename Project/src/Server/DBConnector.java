package Server;

import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

class DBConnector {
	private final static String DB_NAME = "AirlineRegistration";
	private final static String FLIGHT_TABLE = "Flights";
	private final static String TICKET_TABLE = "Tickets";
	private final static String LOGIN_TABLE = "Login";
	
	private final static String USER = "root"; 
	private final static String PASSWORD = "root"; 
	
	// Attributes 
	private Connection connect; 
	private Statement statement; 
	private ResultSet resultSet;
		
	public void initializeConnection() { 
		    try { 
		      // Register JDBC driver
		      Driver driver = new com.mysql.jdbc.Driver(); 
		      DriverManager.registerDriver(driver);
		      
		      // Open a connection
		     // String url = "jdbc:mysql:@" + HOSTNAME + ":" + PORT + ":" + SID; 
		      String url = "jdbc:mysql://localhost:3306/AirlineRegistration?verifyServerCertificate=false&useSSL=true";
		      connect = DriverManager.getConnection(url, USER, PASSWORD);
		      statement = connect.createStatement();
		      
		      // Create database
		      int Result=statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + DB_NAME + ";");
		      
		      // Create flight table
		      String create_flight_table = "CREATE TABLE IF NOT EXISTS " + FLIGHT_TABLE + " ("
		      		+ " FlightNum int AUTO_INCREMENT NOT NULL,"
		      		+ " Source varchar(20) NOT NULL,"
			    	+ " Destination varchar(20) NOT NULL,"
			    	+ " Date datetime,"
			    	+ " DurationMinutes int(3) NOT NULL,"
			    	+ " TotalSeats int(3) NOT NULL,"
			    	+ " RemainingSeats int(3) NOT NULL,"
			    	+ " Price double(6,2) NOT NULL,"
			    	+ " PRIMARY KEY(FlightNum));";
		      statement.executeUpdate(create_flight_table);
		      
		      // Create ticket table
		     String create_ticket_table = "CREATE TABLE IF NOT EXISTS " + TICKET_TABLE + " ("
		    		    + " TicketID int NOT NULL AUTO_INCREMENT,"
		    		  	+ " Firstname varchar(20) NOT NULL,"
		    		  	+ " Lastname varchar(20) NOT NULL,"
		    		  	+ " Birthday varchar(20) NOT NULL,"
			      		+ " FlightNum int(4) NOT NULL,"
			      		+ " Source varchar(20) NOT NULL,"
				    	+ " Destination varchar(20) NOT NULL,"
				    	+ " Date datetime,"
				    	+ " DurationMinutes int(3) NOT NULL,"
				    	+ " Price double(6,2) NOT NULL,"
				    	+ " PRIMARY KEY(TicketID));";
		      statement.executeUpdate(create_ticket_table);
		      
		    } catch (SQLException e) { 
		      e.printStackTrace(); 
		    } 
		  }
	// Search for ticket based on ticket ID
	public ArrayList<Ticket> searchTickets(int TicketID){
		ArrayList<Ticket> retList = new ArrayList<Ticket>();
		try{
			String query = "SELECT * FROM Ticketss where TicketID = ?";
			PreparedStatement pStat = connect.prepareStatement(query);
			if (String.valueOf(TicketID) == "") {
				pStat.setString(1, "*");
			}
			else {
				pStat.setInt(1, TicketID);      
			}
		    resultSet = pStat.executeQuery(); 
			while(resultSet.next()){
				Ticket tempTicket = new Ticket();
				tempTicket.setFlightNum(resultSet.getInt("TicketID"));
				tempTicket.getPassenger().setFirstname(resultSet.getString("Firstname"));
				tempTicket.getPassenger().setLastname(resultSet.getString("Lastname"));
				tempTicket.getPassenger().setBirthday(resultSet.getDate("Birthday"));
				tempTicket.setFlightNum(resultSet.getInt("FlightNum"));
				tempTicket.setFlightSrc(resultSet.getString("Source"));
				tempTicket.setFlightDest(resultSet.getString("Destination"));
				tempTicket.setFlightDate(resultSet.getDate("Date"));
				tempTicket.setFlightDuration(resultSet.getInt("DurationMinutes"));
				tempTicket.setTicketPrice(resultSet.getDouble("Price"));
				
				retList.add(tempTicket);
				pStat.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retList;
	}
	// Search of flights based on origin, destination and date of flight
	public ArrayList<Flight> searchFlights(String Source, String Destination, Date Date){
		ArrayList<Flight> retList = new ArrayList<Flight>();
		try{
			String query = "SELECT * FROM Flights where Source = ? and "
					+ "Destination = ? and Date = ?"; 
			PreparedStatement pStat = connect.prepareStatement(query);
			if (Source == null) {
				pStat.setString(1, "*");
			}
			else pStat.setString(1, Source);
			
			if (Destination == null) {
				pStat.setString(2, "*");
			}
			else pStat.setString(2, Destination);
		    	     
		    if (Date == null) {
		    	pStat.setString(3, "*");
		    }
		    else pStat.setDate(3, Date);
		    
		    resultSet = pStat.executeQuery(); 
			while(resultSet.next()){
				Flight tempFlight = new Flight();
				tempFlight.setFlightNum(resultSet.getInt("FlightNum"));
				tempFlight.setSource(resultSet.getString("Source"));
				tempFlight.setDestination(resultSet.getString("Destination"));
				tempFlight.setDepartureTime(resultSet.getDate("Date"));
				tempFlight.setDurationMinutes(resultSet.getInt("DurationMinutes"));
				tempFlight.setTotalSeats(resultSet.getInt("TotalSeats"));
				tempFlight.setRemainingSeats(resultSet.getInt("RemainingSeats"));
				tempFlight.setPrice(resultSet.getDouble("Price"));
				
				retList.add(tempFlight);
				pStat.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retList;
	}
	// Add flight
	public void insertFlight(int flightNum, String source, String destination, 
			Date date, int durationMinutes, int totalSeats, 
			int remainingSeats, double price) {
		try { 
		      String query = 
		    		  "INSERT INTO Flights "
		      		+ "(FlightNum, Source, Destination, Date, DurationMinutes, "
		      		+ "TotalSeats, RemainingSeats, Price) "
		      		+ "values (?,?,?,?,?,?,?,?)"; 
		      PreparedStatement pStat = connect.prepareStatement(query); 
		      pStat.setInt(1, flightNum);
		      pStat.setString(2, source); 
		      pStat.setString(3, destination);
		      pStat.setDate(4, date);
		      pStat.setInt(5, durationMinutes);
		      pStat.setInt(6, totalSeats);
		      pStat.setInt(7, remainingSeats);
		      pStat.setDouble(8, price);
		      
		      int rowCount = pStat.executeUpdate(); 
		      System.out.println("row Count = " + rowCount); 
		      pStat.close();
		} catch (SQLException e) { 
			      e.printStackTrace(); 
		}
	}
	// Add ticket
	public void insertTicket(int ticketID, String firstname, String lastname,
			Date birthday, int flightNum, String source, String destination,
			Date date, int durationMinutes, double price) {
		try { 
		      String query = 
		    		  "INSERT INTO Tickets "
		      		+ "(TicketID, Firstname, Lastname, Birthday, "
		      		+ "FlightNum, Source, Destination, Date, "
		      		+ "DurationMinutes, Price) "
		      		+ "values (?,?,?,?,?,?,?,?,?,?)"; 
		      PreparedStatement pStat = connect.prepareStatement(query); 
		      pStat.setInt(1, ticketID);
		      pStat.setString(2, firstname); 
		      pStat.setString(3, lastname);
		      pStat.setDate(4, birthday);
		      pStat.setInt(5, flightNum);
		      pStat.setString(6, source);
		      pStat.setString(7, destination);
		      pStat.setDate(8, date);
		      pStat.setInt(9, durationMinutes);
		      pStat.setDouble(10, price);
		      
		      int rowCount = pStat.executeUpdate(); 
		      System.out.println("row Count = " + rowCount); 
		      pStat.close();
		} catch (SQLException e) { 
			      e.printStackTrace(); 
		}
	}
	// Delete flight based on flight number
	public void deleteFlight(int flightNum) { 
		  try { 
		      String delete = "DELETE FROM Flights WHERE FlightNum = ?";
		      PreparedStatement pStat = connect.prepareStatement(delete); 
		      pStat.setInt(1, flightNum);
		      int rowCount = pStat.executeUpdate(); 
		      System.out.println("row Count = " + rowCount);
		      pStat.close();
		  } catch (SQLException e) { 
		      e.printStackTrace(); 
		  }
	}
	// Delete ticket based on ticket ID
	public void deleteTicket(int ticketID) {
		try {
			  // Obtain flight number to update remaining seat
		      String query = "SELECT * FROM Tickets where TicketID = ?";
		      PreparedStatement pStat = connect.prepareStatement(query);
		      resultSet = pStat.executeQuery();
		      int flightNum = resultSet.getInt("FlightNum");
		      int remainingSeats = resultSet.getInt("RemainingSeats");
		      remainingSeats--;
		      
		      // Delete ticket
		      String delete = "DELETE FROM Tickets WHERE TicketID = ?";
		      pStat = connect.prepareStatement(delete); 
		      pStat.setInt(1, ticketID);
		      int rowCount = pStat.executeUpdate(); 
		      System.out.println("row Count = " + rowCount);
		      
		      // Update remaining seat
		      String update = 
		    		  "UPDATE Flights"
		    		+ "SET RemainingSeats = ?"
		    		+ "where FlightNum = ?"; 
		      pStat = connect.prepareStatement(update); 
		      pStat.setInt(1, remainingSeats); 
		      pStat.setInt(2, flightNum);

		      pStat.close();
     
		  } catch (SQLException e) { 
		      e.printStackTrace(); 
		  }
	}
	
	public static void main(String[] args) {
		DBConnector connector = new DBConnector(); 
	    connector.initializeConnection();
	}

}