package Client;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.toedter.calendar.JDateChooser;

import Server.Flight;
import Server.Ticket;

public class Passenger extends Client implements ListSelectionListener {
	
	private JFrame frame = new JFrame();
	
    protected DefaultListModel<String> listModel = new DefaultListModel<String>();
    protected JList<String> listArea = new JList<String>(listModel);
    
	private JTextField searchDate, searchLocationFrom, searchLocationTo;
	private JTextField [] FlightSearch = new JTextField [8];
	private JButton btnViewFlight;
	
	public Passenger(){
		super("localhost", 9898);
		communicate();
	}
	
	public Passenger(int boundW, int boundS){
		super("localhost", 9898);
		communicate();
		frame.setBounds(boundW,boundW,boundS,boundS);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		initialize();
		frame.setVisible(true);
	}

	public void initialize(){
		JLabel labelTitle = new JLabel("ARS Passenger");
		labelTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		labelTitle.setBounds(168, 11, 158, 25);
		frame.getContentPane().add(labelTitle);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JTextField origin = new JTextField(), destination = new JTextField();
				JDateChooser dateSearch = new JDateChooser();
				
				Object [] message = {
						"Origin: ", origin,
						"Destination: ", destination,
						"Date: ", dateSearch
				};
				while(true){
					int result = JOptionPane.showConfirmDialog(null, message, "New Search", JOptionPane.OK_CANCEL_OPTION);
					if(result == JOptionPane.OK_OPTION){
						if(!entryError(origin.getText(), destination.getText(), dateSearch.getDate())){
							searchDate.setText(String.valueOf(dateSearch.getDate()));
							searchLocationFrom.setText(origin.getText());
							searchLocationTo.setText(destination.getText());
							
							listModel.removeAllElements();
							searchFlights(dateSearch.getDate(), origin.getText(), destination.getText());
							updateList();
							break;
						}
					}else if (result == JOptionPane.CANCEL_OPTION) break;
				}
			}
		});
		btnSearch.setBounds(10, 107, 89, 23);
		frame.getContentPane().add(btnSearch);
		
		String [] searchTagNames = {"Flight Number:", "Source", "Destination", "Date", "Time", "Duration", "Prince","Flight Status"};
		JLabel [] searchTag = new JLabel[8];
		
		int startY = 131;
		for(int i = 0; i < searchTag.length; i++){
			searchTag[i] = new JLabel(searchTagNames[i]);
			searchTag[i].setBounds(261, startY, 89, 14);
			startY += 25;
			frame.getContentPane().add(searchTag[i]);
		}
		
		startY = 133;
		for(int i = 0; i < FlightSearch.length; i++){
			FlightSearch[i] = new JTextField();
			FlightSearch[i].setEditable(false);
			FlightSearch[i].setBounds(363,startY,106,20);
			frame.getContentPane().add(FlightSearch[i]);
			FlightSearch[i].setColumns(10);
			startY+=25;
		}
		
		JButton btnClearSearch = new JButton("Clear");
		btnClearSearch.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				for(int i = 0; i< FlightSearch.length; i++){
					FlightSearch[i].setText("");
				}
			}
		});
		btnClearSearch.setBounds(380, 336, 89, 23);
		frame.getContentPane().add(btnClearSearch);
		
		searchDate = new JTextField();
		searchDate.setEditable(false);
		searchDate.setBounds(47, 76, 194, 20);
		frame.getContentPane().add(searchDate);
		searchDate.setColumns(10);
		
		JLabel lblFrom = new JLabel("Date:");
		lblFrom.setLabelFor(searchDate);
		lblFrom.setBounds(10, 79, 37, 14);
		frame.getContentPane().add(lblFrom);
		
		JLabel lblOrigin = new JLabel("Origin:");
		lblOrigin.setBounds(261, 58, 37, 14);
		frame.getContentPane().add(lblOrigin);
		
		searchLocationFrom = new JTextField();
		searchLocationFrom.setEditable(false);
		searchLocationFrom.setColumns(10);
		searchLocationFrom.setBounds(336, 55, 133, 20);
		frame.getContentPane().add(searchLocationFrom);
		
		JLabel lblDestination_1 = new JLabel("Destination:");
		lblDestination_1.setBounds(261, 86, 78, 14);
		frame.getContentPane().add(lblDestination_1);
		
		searchLocationTo = new JTextField();
		searchLocationTo.setEditable(false);
		searchLocationTo.setColumns(10);
		searchLocationTo.setBounds(336, 83, 133, 20);
		frame.getContentPane().add(searchLocationTo);
		
		JButton btnBookFlight = new JButton("Book Flight");
		btnBookFlight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JTextField firstName = new JTextField();
				JTextField lastName = new JTextField();
				JTextField birthday = new JTextField();
				
				Object [] message = {
						"FirstName:", firstName,
						"LastName:", lastName,
						"Birthday:", birthday
				};
				while(true){
					int result = JOptionPane.showConfirmDialog(null, message, "User Profile", JOptionPane.OK_CANCEL_OPTION);
					if(result == JOptionPane.OK_OPTION){
						if(firstName.getText().isEmpty() || lastName.getText().isEmpty()){
							JOptionPane.showMessageDialog(null, "Name fields cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
						}else{
							bookTicket(Integer.parseInt(searchTag[0].getText()), firstName.getText(), lastName.getText(), birthday.getText());
							String response;
							try {
								response = socketIn.readLine();
								if(response.equalsIgnoreCase("PASS")){
									JOptionPane.showMessageDialog(null, "Ticket booked and saved", "Ticket Confirmation", JOptionPane.PLAIN_MESSAGE);
									break;
								}
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					}else if(result == JOptionPane.CANCEL_OPTION) break;
				}
			}
		});
		btnBookFlight.setBounds(261, 336, 109, 23);
		frame.getContentPane().add(btnBookFlight);
		
		btnViewFlight = new JButton("View Flight");
		btnViewFlight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = listArea.getSelectedIndex();
				System.out.println(index);
				String [] flightInfo = listFlights.getElementAt(index).split(" ; ");			
				
				String oldDate = flightInfo[3];
				System.out.println(oldDate);
				SimpleDateFormat originalFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
				SimpleDateFormat targetDateFormat = new SimpleDateFormat("yyyy.MM.dd G");
				SimpleDateFormat targetTimeFormat = new SimpleDateFormat("K:mm a, z");
				
				Date date = null;
				Date time = null;
				
				try {
					time = originalFormat.parse(oldDate);
					date = originalFormat.parse(oldDate);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				
				String formattedDate = targetDateFormat.format(date);
				String formattedTime = targetTimeFormat.format(time);
				
				for(int i = 0; i <flightInfo.length; i++){
					if(i != 3 || i != 4){
						FlightSearch[i].setText(flightInfo[i]);
					}
				}
				
				FlightSearch[3].setText(formattedDate);
				FlightSearch[4].setText(formattedTime);
			}
		});
		btnViewFlight.setBounds(143, 107, 98, 23);
		frame.getContentPane().add(btnViewFlight);
		
		listArea.setVisibleRowCount(8);
		listArea.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane listPanel = new JScrollPane(listArea);
		listPanel.setBounds(10, 135, 231, 315);
		frame.getContentPane().add(listPanel);
		
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				closeConnection();
			}
		});
	}
	
	public void updateList(){
		listModel.clear();
		for(int i = 0; i < listFlights.size(); i++){
			listModel.addElement(listFlights.getElementAt(i));
		}
	}
	
	public boolean entryError(String src, String dest, Date dateSearched){
		if(src.matches(".*\\d+.*") || dest.matches(".*\\d+.*")){
			JOptionPane.showMessageDialog(null, "Location must be strings","Error", JOptionPane.ERROR_MESSAGE);
			return true;
		}else if(src.equals(dest) && !src.isEmpty()){
			JOptionPane.showMessageDialog(null, "Source and Destination cannot be the same","Error", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		String dateToday = String.valueOf(new Date());
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		try {
			Date currentDate = (Date)formatter.parse(dateToday);
			if(dateSearched.before(currentDate)) {
				JOptionPane.showMessageDialog(null, "The entered date has passed. Please try again", "Error", JOptionPane.ERROR_MESSAGE);
				return true;
			}
		}catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {}
	
	public static void main(String[] args) {
		Passenger newPassenger = new Passenger(100,500);
	}
}





