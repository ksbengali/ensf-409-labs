package Client;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import com.toedter.calendar.JDateChooser;

import Server.Flight;
import Server.Ticket;

public class Admin extends Client{
	private JFrame frame = new JFrame();
	private JList list;
	private DefaultListModel listModel;
	private DefaultListModel<String> listTickets = new DefaultListModel<String>();
	
	public Admin(){
		super("localhost", 9898);
		communicate();
		frame.setBounds(100,100,450,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		initialize();
		frame.setVisible(true);
	}
	
	public void initialize(){
		JLabel Title = new JLabel("ARS Admin");
		Title.setFont(new Font("Dialog", Font.BOLD, 20));
		Title.setBounds(157, 10, 119, 22);
		frame.getContentPane().add(Title);
		
		JLabel label_1 = new JLabel("Booked Tickets");
		label_1.setBounds(10, 39, 145, 22);
		frame.getContentPane().add(label_1);
		
		listModel = new DefaultListModel();
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(0);
		//list.addMouseMotionListener((MouseMotionListener) this);
		list.setVisibleRowCount(8);
		JScrollPane listScrollPane = new JScrollPane(list);
		listScrollPane.setBounds(10, 61, 207, 190);
		frame.getContentPane().add(listScrollPane);
		
		JButton btn_cancelTicket = new JButton("Cancel");
		btn_cancelTicket.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		btn_cancelTicket.setBounds(223, 106, 93, 22);
		frame.getContentPane().add(btn_cancelTicket);
		
		JLabel label_2 = new JLabel("Ticket Id:");
		label_2.setBounds(223, 61, 62, 22);
		frame.getContentPane().add(label_2);
		
		JTextField ticketIdTextField = new JTextField();
		ticketIdTextField.setEditable(false);
		ticketIdTextField.setBounds(291, 61, 133, 22);
		frame.getContentPane().add(ticketIdTextField);
		
		JButton btn_clearId = new JButton("Clear");
		btn_clearId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ticketIdTextField.setText("");
			}
		});
		btn_clearId.setBounds(331, 106, 93, 22);
		frame.getContentPane().add(btn_clearId);
		
		JButton btn_addFlight = new JButton("Add Flight");
		btn_addFlight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addFlight();
			}
		});
		btn_addFlight.setBounds(223, 134, 93, 22);
		frame.getContentPane().add(btn_addFlight);
		
		JButton btn_loadFlights = new JButton("Load");
		btn_loadFlights.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String filename = JOptionPane.showInputDialog(null, "File Name:", "Load Flights",JOptionPane.PLAIN_MESSAGE);
				System.out.println(filename);
			}
		});
		btn_loadFlights.setBounds(331, 134, 93, 22);
		frame.getContentPane().add(btn_loadFlights);
		
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				closeConnection();
			}
		});
	}
	
	public void addFlight(){
		JTextField [] flightParams = new JTextField [7];
		
		for(int i = 0; i < flightParams.length; i++){
			flightParams[i] = new JTextField();
		}
		JDateChooser flightDate = new JDateChooser();
		
		Object [] message = {
				"Flight Number: ", flightParams[0],
				"Flight Source: ", flightParams[1],
				"Flight Destination: ", flightParams[2],
				"Flight Date: ", flightDate,
				"Flight Departure Time: ", flightParams[3],
				"Flight Duration: ", flightParams[4],
				"Total Seats: ", flightParams[5],
				"Flight Price: ", flightParams[6]
		};
		while(true){
			int result = JOptionPane.showConfirmDialog(null, message, "Add Flight", JOptionPane.OK_CANCEL_OPTION);
			String [] newFlight = new String [8];
			if(result == JOptionPane.OK_OPTION){
				for(int i = 0; i < newFlight.length-1; i++){
					newFlight[i] = flightParams[i].getText(); 
				}
				newFlight[newFlight.length-1] = String.valueOf(flightDate.getDate());
				if(!entryError(newFlight)){
					System.out.println(newFlight[3]);
					break;
				}
			}else if(result == JOptionPane.CANCEL_OPTION) break;
		}
	}
	
	private void addFlight(String flightId, String source, String destination, Date departure,
			int flightDuration, int totalSeats, int RemaningSeats, double price){
		socketOut.flush();
		socketOut.println("ADD_FLIGHT");
		socketOut.println(flightId);
		socketOut.println(source);
		socketOut.println(destination);
		socketOut.println(departure);
		socketOut.println(flightDuration);
		socketOut.println(totalSeats);
		socketOut.println(RemaningSeats);
		socketOut.println(price);
	}
	
	private void cancelTicket(String ticketId){
		socketOut.flush();
		socketOut.println(ticketId);
	}
	
	private void viewAllTickets(){
		socketOut.flush();
		socketOut.println("VIEW_TICKETS");
		deserializeTicketList();
	}
	
	private void deserializeTicketList(){
		listTickets.clear();
		try{
			listTickets = (DefaultListModel<String>) objectIn.readObject();
		}catch(IOException | ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	public boolean entryError(String [] arg){
		// Flight Number, Seats, and Price are not Integers
		try{ 
			Integer.parseInt(arg[0]);
			Integer.parseInt(arg[5]);
			Integer.parseInt(arg[6]);
		}catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "FlightNumber/Seats/Price must be an Integer", "Error", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		// Source and Destination are the same or if they contain numbers
		if(arg[1].equalsIgnoreCase(arg[2])){
			JOptionPane.showMessageDialog(null, "Source and Destination cannot be the same", "Error", JOptionPane.ERROR_MESSAGE);
			return true;
		}else if(arg[2].matches(".*\\d+.*") || arg[1].matches(".*\\d+.*")) {
			JOptionPane.showMessageDialog(null, "Source or Destination must be strings", "Error", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		// Invalid Date
		String dateToday = String.valueOf(new Date());
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		try {
			Date currentDate = (Date)formatter.parse(dateToday);
			Date enteredDate = (Date)formatter.parse(arg[7]);
			if(enteredDate.before(currentDate)) {
				JOptionPane.showMessageDialog(null, "The entered date has passed. Please try again", "Error", JOptionPane.ERROR_MESSAGE);
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void main(String[] args) {
		Admin newAdmin = new Admin();
	}

}
