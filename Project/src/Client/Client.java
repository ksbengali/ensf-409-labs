package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.DefaultListModel;

import Server.Flight;
import Server.Ticket;

public class Client {
	private 	Socket 				aSocket;
	protected 	BufferedReader 		socketIn;
	protected 	PrintWriter 		socketOut;
	private 	ObjectOutputStream 	objectOut;
	protected 	ObjectInputStream 	objectIn;
	private 	String 				host;
	private 	int 				port;
	
	protected ArrayList<Flight>flights = new ArrayList<Flight>();
	protected ArrayList<Ticket>tickets = new ArrayList<Ticket>();
	protected DefaultListModel <String> listFlights = new DefaultListModel<String>();
	
	public Client(String h, int p){
		this.host = h;
		this.port = p;
	}
	
	public void connect(){
		try{
			aSocket = new Socket(host, port);
		}catch(IOException e){
			System.out.println("Could not establish connection");
			e.printStackTrace();
		}
	}
	
	public void communicate(){
		this.connect();
		try{
			socketIn	=new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
			socketOut	=new PrintWriter((aSocket.getOutputStream()),true);
			objectOut	=new ObjectOutputStream(aSocket.getOutputStream());
			objectIn	=new ObjectInputStream(aSocket.getInputStream());
			
			System.out.println("Connection Established");
		}catch(IOException e){
			System.out.println("Communication Error");
			e.printStackTrace();
		}
	}
	
	protected void searchFlights(Date date, String origin, String destination){
		socketOut.flush();
		socketOut.println("SEARCH_FLIGHTS");
		socketOut.flush();
		socketOut.println(date);
		socketOut.println(origin);
		socketOut.println(destination);
		socketOut.flush();
		deserializeFlightList();	
	}
	
	protected void bookTicket(int flightId, String firstName, String lastName, String birthday){
		socketOut.flush();
		socketOut.println("BOOK_TICKET");
		socketOut.println(flightId);
		socketOut.println(firstName);
		socketOut.println(lastName);
		socketOut.println(birthday);
	}
	
	private void deserializeFlightList(){
		listFlights.clear();
		try{
			listFlights = (DefaultListModel<String>) objectIn.readObject();
			System.out.println(listFlights.getElementAt(0));
		}catch(IOException | ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	protected void closeConnection(){
		try{
			socketOut.println("QUIT");
			socketIn.close();
			socketOut.close();
			objectOut.close();
			objectIn.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
}
